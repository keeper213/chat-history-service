<?php

class DB
{
	private static $instance;
	private $MySQLi;

	public static function init( array $dbOptions )
	{
		if( self::$instance instanceof self ) {
			return false;
		}

		self::$instance = new self( $dbOptions );

		self::$instance->checkDatabase( $dbOptions[ 'db_name' ] );
		self::$instance->checkTables();
	}

	private function __construct( array $dbOptions )
	{
		$this->MySQLi = @ new mysqli( $dbOptions[ 'db_host' ],
		                              $dbOptions[ 'db_user' ],
		                              $dbOptions[ 'db_pass' ] );

		if( mysqli_connect_error() ) {
			throw new Exception( 'Database connect error ( '
			                     . mysqli_connect_errno() . ' ): '
			                     . mysqli_connect_error() );
		}

		$this->MySQLi->set_charset( "utf8" );
	}

	private function checkDatabase( $dbName )
	{
		if( !$this->MySQLi->select_db( $dbName ) ) {
			$this->createDatabase( $dbName );
			$this->MySQLi->select_db( $dbName );
		}
	}

	private function createDatabase( $dbName )
	{
		if( !$this::query( "CREATE DATABASE IF NOT EXISTS `{$dbName}`" ) )
			throw new Exception( "Cann't create database {$dbName}" );
	}

	private function checkTables()
	{
		if( !$this::query( "CREATE TABLE IF NOT EXISTS users (
		                       id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
		                       name TEXT NOT NULL
		                    ) DEFAULT CHARSET=utf8"
		                 )
		) {
			throw new Exception( "Cann't create table 'users'" );
		}

		if( !$this::query( "CREATE TABLE IF NOT EXISTS messages (
		                       msg_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
		                       room_id INT NOT NULL,
		                       sender_id INT NOT NULL,
		                       time_moment INT NOT NULL,
		                       msg_type INT NOT NULL,
		                       msg_data TEXT,
		                       link TEXT
		                    ) DEFAULT CHARSET=utf8"
		                 )
		) {
			throw new Exception( "Cann't create table 'messages'" );
		}

		if( !$this::query( "CREATE TABLE IF NOT EXISTS rooms (
		                       room_id INT NOT NULL,
		                       user_id INT NOT NULL )"
		                 )
		) {
			throw new Exception( "Cann't create table 'rooms'" );
		}

		if( !$this::query( "CREATE TABLE IF NOT EXISTS message_types (
		                       id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
		                       name TEXT )"
		                 )
		) {
			throw new Exception( "Cann't create table 'message_types'" );
		}
	}

	public static function query( $query )
	{
		return self::$instance->MySQLi->query( $query );
	}

	public static function escape( $str )
	{
		return self::$instance->MySQLi
		                      ->real_escape_string( htmlspecialchars( $str ) );
	}
}
?>
